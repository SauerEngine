// Engine
#include "pch.h"
#include "cube.h"
#include "iengine.h"
#include "igame.h"

// Game
#include "gcommunicator.h"

game_communicator::~game_communicator()
{
}

void game_communicator::gamedisconnect()
{
}
void game_communicator::parsepacketclient(int chan, ucharbuf &p)
{
}
int game_communicator::sendpacketclient(ucharbuf &p, bool &reliable, dynent *d)
{
return -1;
}
void game_communicator::gameconnect(bool _remote)
{
}
bool game_communicator::allowedittoggle()
{
return true;
}
void game_communicator::writeclientinfo(FILE *f)
{
}
void game_communicator::toserver(char *text)
{
}
void game_communicator::changemap(const char *name) 
{
	load_world(name);
}

