// Engine
#include "pch.h"
#include "cube.h"
#include "iengine.h"
#include "igame.h"

// Game
#include "gserver.h"

game_server::~game_server()
{
}

void *game_server::newinfo() { return NULL; }
void game_server::deleteinfo(void *ci) {}
void game_server::serverinit() {}
void game_server::clientdisconnect(int n) {}
int game_server::clientconnect(int n, uint ip) { return DISC_NONE; }
void game_server::localdisconnect(int n) {}
void game_server::localconnect(int n) {}
const char *game_server::servername() { return "foo"; }
void game_server::parsepacket(int sender, int chan, bool reliable, ucharbuf &p) {}
bool game_server::sendpackets() { return false; }
int game_server::welcomepacket(ucharbuf &p, int n, ENetPacket *packet) { return -1; }
void game_server::serverinforeply(ucharbuf &q, ucharbuf &p) {}
void game_server::serverupdate(int lastmillis, int totalmillis) {}
bool game_server::servercompatible(char *name, char *sdec, char *map, int ping, const vector<int> &attr, int np) { return false; }
void game_server::serverinfostr(char *buf, const char *name, const char *desc, const char *map, int ping, const vector<int> &attr, int np) {}
int game_server::serverinfoport() { return 0; }
int game_server::serverport() { return 0; }
const char *game_server::getdefaultmaster() { return "localhost"; }
void game_server::sendservmsg(const char *s) {}
