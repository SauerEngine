// Engine
#include "pch.h"
#include "cube.h"
#include "iengine.h"
#include "igame.h"

// Game
struct game_entities;

#include "gcommunicator.h"
#include "gserver.h"
#include "gclient.h"
#include "gentities.h"

#ifndef STANDALONE // If we are building standalone...

game_client::~game_client() {}

const char *game_client::gameident() { return ""; }
const char *game_client::defaultmap() { return ""; }
const char *game_client::savedconfig() { return ""; }
const char *game_client::defaultconfig() { return ""; }
const char *game_client::autoexec() { return ""; } 
const char *game_client::savedservers() { return NULL; }

icliententities *game_client::getents() { return &gents; } // Segfault TODO: Fix it so it returns an entities class, for reference look at entities.h in fps/rpg game.
iclientcom *game_client::getcom() { return &gcom; }

bool game_client::clientoption(char *arg) { return false; }
void game_client::updateworld(vec &pos, int curtime, int lm) { }
void game_client::initclient() { }
void game_client::physicstrigger(physent *d, bool local, int floorlevel, int waterlevel, int material) { }
void game_client::edittrigger(const selinfo &sel, int op, int arg1, int arg2, int arg3) { }
char *game_client::getclientmap() { return ""; }
void game_client::resetgamestate() { }
void game_client::suicide(physent *d) { }
void game_client::newmap(int size)  { }
void game_client::startmap(const char *name) { }
void game_client::preload() {}
float game_client::abovegameplayhud() { return 1.0f; }
void game_client::gameplayhud(int w, int h)  { }
void game_client::drawhudgun() { }
bool game_client::canjump() { return true; }
bool game_client::allowmove(physent *d) { return true; }
void game_client::doattack(bool on) { }
dynent *game_client::iterdynents(int i) { return NULL; }
int game_client::numdynents() { return 0; }
void game_client::rendergame() {}
void game_client::writegamedata(vector<char> &extras) {}
void game_client::readgamedata(vector<char> &extras) {}
void game_client::g3d_gamemenus() { }
const char *game_client::defaultcrosshair(int index) { return NULL; }
int game_client::selectcrosshair(float &r, float &g, float &b) { return 0; }
void game_client::lighteffects(dynent *d, vec &color, vec &dir) {}
void game_client::setupcamera() {}
bool game_client::detachcamera() { return false; }
void game_client::adddynlights() {}
void game_client::particletrack(physent *owner, vec &o, vec &d) {}
bool game_client::serverinfostartcolumn(g3d_gui *g, int i) { return false; }
void game_client::serverinfoendcolumn(g3d_gui *g, int i) {}
bool game_client::serverinfoentry(g3d_gui *g, int i, const char *name, const char *desc, const char *map, int ping, const vector<int> &attr, int np) { return false; };

// Register the game.
// Argument meanings:
// 1: Name of the game's new struct.
// 2: Name of the game.
// 3: A pointer to the game's client class.
// 4: A pointer to the game's server class.

REGISTERGAME(modgame, "mod", new game_client(), new game_server());

#else

REGISTERGAME(modgame, "mod", NULL, new game_server());

#endif
