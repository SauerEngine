#ifndef GENTITIES_H
#define GENTITIES_H

struct game_client;

struct game_entities : icliententities
{
    entities(game_client &_cl);
    vector<extentity *> &getents();    
    const char *itemname(int i);   
    const char *entmdlname(int type);
    void preloadentities();
    void renderent(extentity &e, const char *mdlname, float z, float yaw);
    void renderent(extentity &e, int type, float z, float yaw);
    void renderentities();
    void rumble(const extentity &e);
    void trigger(extentity &e);
    void addammo(int type, int &v, bool local = true);
    void repammo(fpsent *d, int type);
    void pickupeffects(int n, fpsent *d);
    void teleport(int n, fpsent *d);
    void trypickup(int n, fpsent *d);
    void checkitems(fpsent *d);
    void checkquad(int time, fpsent *d);
    void putitems(ucharbuf &p, int gamemode);
    void resetspawns();
    void spawnitems(int gamemode);
    void setspawn(int i, bool on);
    extentity *newentity();
    void fixentity(extentity &e);
    void entradius(extentity &e, float &radius, float &angle, vec &dir);
    const char *entnameinfo(entity &e);
    const char *entname(int i);
    int extraentinfosize();
    void writeent(entity &e, char *buf);
    void readent(entity &e, char *buf);
    void editent(int i);
    float dropheight(entity &e);

    game_client &cl;  
    vector<extentity *> ents;
};

#endif
