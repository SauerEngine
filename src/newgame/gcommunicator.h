#ifndef GAMECOMMUNICATOR_H
#define GAMECOMMUNICATOR_H

struct game_communicator : iclientcom
{
    ~game_communicator();

    void gamedisconnect();
    void parsepacketclient(int chan, ucharbuf &p);
    int sendpacketclient(ucharbuf &p, bool &reliable, dynent *d);
    void gameconnect(bool _remote);
    bool allowedittoggle();
    void writeclientinfo(FILE *f);
    void toserver(char *text);
    void changemap(const char *name);
};

#endif
