#ifndef GCLIENT_H
#define GCLIENT_H

#ifndef STANDALONE
struct game_client : public igameclient
{
public:
	~game_client();

	const char *gameident();
	const char *defaultmap();
	const char *savedconfig();
	const char *defaultconfig();
	const char *autoexec();
	const char *savedservers();

	icliententities *getents(); // Segfault TODO: Fix it so it returns an entities class, for reference look at entities.h in fps/rpg game.
	iclientcom *getcom();

	bool clientoption(char *arg);
	void updateworld(vec &pos, int curtime, int lm);
	void initclient();
	void physicstrigger(physent *d, bool local, int floorlevel, int waterlevel, int material = 0);
	void edittrigger(const selinfo &sel, int op, int arg1 = 0, int arg2 = 0, int arg3 = 0);
	char *getclientmap();
	void resetgamestate();
	void suicide(physent *d);
	void newmap(int size);
	void startmap(const char *name);
	void preload();
	float abovegameplayhud();
	void gameplayhud(int w, int h);
	void drawhudgun();
	bool canjump();
	bool allowmove(physent *d);
	void doattack(bool on);
	dynent *iterdynents(int i);
	int numdynents();
	void rendergame();
	void writegamedata(vector<char> &extras);
	void readgamedata(vector<char> &extras);
	void g3d_gamemenus();
	const char *defaultcrosshair(int index);
	int selectcrosshair(float &r, float &g, float &b);
	void lighteffects(dynent *d, vec &color, vec &dir);
	void setupcamera();
	bool detachcamera();
	void adddynlights();
	void particletrack(physent *owner, vec &o, vec &d);
	bool serverinfostartcolumn(g3d_gui *g, int i);
	void serverinfoendcolumn(g3d_gui *g, int i);
	bool serverinfoentry(g3d_gui *g, int i, const char *name, const char *desc, const char *map, int ping, const vector<int> &attr, int np);

private:
	game_communicator gcom;
	game_entities gents;
};
#endif

#endif
