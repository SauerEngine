#ifndef AGAMESERVER_H
#define AGAMESERVER_H

struct game_server : igameserver
{
    ~game_server();

    void *newinfo();
    void deleteinfo(void *ci);
    void serverinit();
    void clientdisconnect(int n);
    int clientconnect(int n, uint ip);
    void localdisconnect(int n);
    void localconnect(int n);
    const char *servername();
    void parsepacket(int sender, int chan, bool reliable, ucharbuf &p);
    bool sendpackets();
    int welcomepacket(ucharbuf &p, int n, ENetPacket *packet);
    void serverinforeply(ucharbuf &q, ucharbuf &p);
    void serverupdate(int lastmillis, int totalmillis);
    bool servercompatible(char *name, char *sdec, char *map, int ping, const vector<int> &attr, int np);
    void serverinfostr(char *buf, const char *name, const char *desc, const char *map, int ping, const vector<int> &attr, int np);
    int serverinfoport();
    int serverport();
    const char *getdefaultmaster();
    void sendservmsg(const char *s);
};

#endif
